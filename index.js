"use strict";
/*!
 *  MatomoOptOut
 *
 *  @copyright Copyright (c) 2020, Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
exports.__esModule = true;
exports.MatomoOptOut = void 0;
var MatomoOptOut_1 = require("./src/MatomoOptOut");
__createBinding(exports, MatomoOptOut_1, "MatomoOptOut");
