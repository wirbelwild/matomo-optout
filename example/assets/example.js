import "./example.scss";
import { MatomoOptOut } from "../../index";

const matomoOptOut = new MatomoOptOut(
    "https://matomo.bitandblack.com",
    document.querySelector("#tracking-enabled"),
    document.querySelector("#tracking-disabled")
);

matomoOptOut
    .setErrorMessage(
        document.querySelector("#tracking-error")
    )
    .setErrorCallback(() => {
        console.error("Could not change tracking status.");
    })
    .setOnStatusChangeCallback((isTrackingActive) => {
        console.log(`Status has changed. Tracking is active: ${isTrackingActive}`);
    })
;