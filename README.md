[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d7cf8ba71f1841488934cd959d59328a)](https://www.codacy.com/app/bitandblack/matomo-optout?utm_source=wirbelwild@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/matomo-optout&amp;utm_campaign=Badge_Grade)
[![License](https://poser.pugx.org/bitandblack/matomooptout/license)](https://packagist.org/packages/bitandblack/matomooptout)
[![PHP version](https://badge.fury.io/ph/bitandblack%2Fmatomooptout.svg)](https://badge.fury.io/ph/bitandblack%2Fmatomooptout)
[![npm version](https://badge.fury.io/js/bitandblack-matomo-optout.svg)](https://badge.fury.io/js/bitandblack-matomo-optout)

# MatomoOptOut

Custom OptOut in Matomo (Piwik) with AJAX. Doesn't need an iframe.

MatomoOptOut makes use of the Matomo (Piwik) plugin [Ajax Opt Out](https://plugins.matomo.org/AjaxOptOut) from »Lipperts Web«. You need to install and activate it too.

## Installation

This package is available for [Composer](https://packagist.org/packages/bitandblack/matomooptout) and also for [Node](https://www.npmjs.com/package/bitandblack-matomo-optout).

### Composer 

Install __MatomoOptOut__ by running `$ composer require bitandblack/matomo-optout`.

### NPM

Install __MatomoOptOut__ by running `$ npm install bitandblack-matomo-optout`. 

### Yarn 

Install __MatomoOptOut__ by running `$ yarn add bitandblack-matomo-optout`.

## Getting started 

Initialize MatomoOptOut like that:

````javascript 
import { MatomoOptOut } from "bitandblack-matomooptout";

const matomoOptOut = new MatomoOptOut(
    "https://yoursite.com/matomo",
    document.querySelector("#tracking-enabled"),
    document.querySelector("#tracking-disabled")
);
````

The first parameter is the URL to your Matomo instance. 

The second parameter is the HTML element, that holds the text when the tracking is __enabled__. It should contain also a checkbox to disable the tracking.

The third parameter is the HTML element, that holds the text when the tracking is __disabled__. It should contain also a checkbox to enable the tracking.

How does the HTML look like? Continue reading:

## HTML 

The HTML when tracking is enabled could look like that:

````html
<div id="tracking-enabled">
    <p>You may choose to prevent this website from aggregating and analyzing the actions you take here. Doing so will protect your privacy, but will also prevent the owner from learning from your actions and creating a better experience for you and other users.</p>
    <p>
        <input id="disable-tracking" type="checkbox" checked="checked">
        <label for="disable-tracking">You are not opted out. Uncheck this box to opt-out.</label>
    </p>
</div>
````

The HTML when tracking is disabled could look like that:

````html
<div id="tracking-disabled">
    <p>Opt-out complete; your visits to this website will not be recorded by the Web Analytics tool. Note that if you clear your cookies, delete the opt-out cookie, or if you change computers or Web browsers, you will need to perform the opt-out procedure again.</p>
    <p>
        <input id="enable-tracking" type="checkbox">
        <label for="enable-tracking">You are currently opted out. Check this box to opt-in.</label>
    </p>
</div>
````

## Options 

### Error message 

You can define an error message for the case changing the tracking status fails. This may happen if the browser blocks third party cookies. 

An error message could look like that:

````html
<div id="tracking-error">
    <p>Unfortunately the status cannot be changed. This can be caused by the data protection setting of your browser. Please change the status under <a href="https://yoursite.com/matomo/index.php?module=CoreAdminHome&action=optOut&language=en" target="_blank" rel="noopener nofollow">https://yoursite.com/matomo</a>.</p>
</div>
````

Add that to the script by calling

````javascript
matomoOptOut.setErrorMessage(
    document.querySelector("#tracking-error")
);
````

### Error callback

Instead of showing an error by displaying a piece of HTML, you can get that information directly:

````javascript
matomoOptOut.setErrorCallback(() => {
    console.error("Could not change tracking status.");
});
````

### Continuous tracking status check 

You can allow the library to continuously check the tracking status:

````javascript
matomoOptOut.watchStatusChange(10);
````

The first parameter is the time of the interval in seconds.

## Help 

Feel free to contact us under `hello@bitandblack.com`.
