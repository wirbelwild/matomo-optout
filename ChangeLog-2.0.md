# Changes in Bit&Black Matomo OptOut 2.0

## 2.0.1 2020-12-15

### Fixed

-   Fixed error when no error message was defined.

## 2.0.0 2020-12-10

### Changed

-   This library is no longer a jQuery plugin and can be installed without it. Please take a look at the [ReadMe.md](README.md) to find out how it needs to be configured now.

### Added 

-   A custom error message can be displayed.
-   Error messages can be received over a callback.
-   Status changes can be received over a callback.