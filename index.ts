/*!
 *  MatomoOptOut
 *
 *  @copyright Copyright (c) 2020, Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */

export { MatomoOptOut } from "./src/MatomoOptOut";