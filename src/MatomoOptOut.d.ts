/**
 *  MatomoOptOut
 *
 *  @copyright Copyright (c) 2020, Bit&Black
 *  @author Tobias Köngeter <hello@bitandblack.com>
 *  @link https://www.bitandblack.com
 */
/**
 * MatomoOptOut.
 */
declare class MatomoOptOut {
    /**
     * The root URL to Matomo.
     *
     * @private
     */
    private readonly matomoRoot;
    /**
     * The whole tracking URL.
     *
     * @private
     */
    private readonly matomoTrackingURL;
    /**
     * The current tracking status.
     *
     * @private
     */
    private isTrackingActive;
    /**
     * The HTML element for error messages.
     *
     * @private
     */
    private errorMessage;
    /**
     * The interval for fetching the status.
     *
     * @private
     */
    private interval;
    /**
     * The HTML element when tracking is enabled.
     *
     * @private
     */
    private readonly trackingEnabledElement;
    /**
     * The HTML element when tracking is disabled.
     *
     * @private
     */
    private readonly trackingDisabledElement;
    /**
     * The callback when an error appears.
     *
     * @private
     */
    private errorCallback;
    /**
     * The callback when the status has changed.
     *
     * @private
     */
    private onStatusChangeCallback;
    /**
     * The CSS class to make an element visible.
     *
     * @private
     */
    private classElementIsVisible;
    /**
     * The CSS class to make an element invisible.
     *
     * @private
     */
    private classElementIsInvisible;
    /**
     * If classes should be used to show or hide HTML.
     *
     * @private
     */
    private useClassesForVisibility;
    /**
     * Constructor.
     *
     * @param matomoRoot              The root URL to your Matomo instance.
     * @param trackingEnabledElement  The html element holding text and checkbox when tracking is enabled.
     * @param trackingDisabledElement The html element holding text and checkbox when tracking is disabled.
     */
    constructor(matomoRoot: string, trackingEnabledElement: HTMLElement, trackingDisabledElement: HTMLElement);
    /**
     * Returns if a element is child of another element.
     *
     * @param child
     * @param parent
     */
    private isChildOf;
    /**
     * Updates the HTML output. This will be called as a callback after requesting the tracking status.
     *
     * @param response
     */
    private updateHTMLOutput;
    /**
     * Gets the matomo tracking status.
     *
     * @param callback The callback when requesting the tracking status.
     */
    getMatomoTrackingStatus: (callback?: (response: any) => void) => void;
    /**
     * Sets the tracking status.
     *
     * @param status   The new status
     * @param callback The callback when the changing the status
     */
    private setMatomoTrackingStatus;
    /**
     * Checks if the status changed.
     *
     * @param statusNew
     */
    private checkStatusChange;
    /**
     * Sets an HTML element with a custom error message in case the tracking status could not changed.
     *
     * @param element
     */
    setErrorMessage: (element: HTMLElement) => this;
    /**
     * Sets a custom error callback.
     *
     * @param errorCallback
     */
    setErrorCallback: (errorCallback: () => void) => this;
    /**
     * Sets a custom callback for when the tracking status has changed.
     *
     * @param onStatusChangeCallback
     */
    setOnStatusChangeCallback: (onStatusChangeCallback: (isTrackingActive: boolean) => void) => this;
    /**
     * Sets CSS classes to handle the visibility of the HTML elements.
     *
     * @param classElementIsVisible   The class name for visible elements.
     * @param classElementIsInvisible The class name for invisible elements.
     */
    setCSSClasses: (classElementIsVisible: string, classElementIsInvisible: string) => this;
    /**
     * Enables a constant check for the current tracking status.
     *
     * @param seconds The interval in seconds.
     */
    watchStatusChange: (seconds: number) => this;
    /**
     * Concat multiple selectors together.
     *
     * @param elements
     */
    private getElements;
    private setErrorMessageVisibility;
}
export { MatomoOptOut };
